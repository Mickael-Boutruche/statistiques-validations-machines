<?php

// <----- Auteur : Mickaël Boutruche ----->
// <----- Date : 28 mars 2017 ----->
require './fonctions/fonctions.php';
?>
<!DOCTYPE HTML>
<html>
    <head>    
        <link rel="stylesheet" href="css/style.css" />
        <title>Statistiques des machines</title>
    </head>
    <body>
        <form method='POST' action="index.php">
            <div class="poste-name">              
                <?php 
                    list_postes();
                    list_choix_jour();        
                ?>
            <input type="submit" value="Generer les statistiques"/>
            </div>            
        </form>
        <div class="generer-graph"><br />        
            <?php             
            if (isset($_POST['poste_name']))
            {
                generer_graph();               
                echo '<object data="graph.php" width="1000" height="400" type="image/svg+xml" />';
            }
            ?>
        </div>
    </body>
</html>

