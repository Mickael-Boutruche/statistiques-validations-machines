<?php

// <----- Auteur : Mickaël Boutruche ----->
// <----- Date : 28 mars 2017 ----->

function connexion() {

    $host = "172.16.99.2"; //adresse de connexion à la BDD
    $user = "m.boutruche"; // nom d'utilsiateur à la BDD
    $pass = "P@ssword"; //mot de passe à la BDD
    $db = "examen_epreuve_e4"; // nom de la base de données

    try {
        // tentative de connexion à la BDD
        $dbh = new PDO("pgsql:host=$host;port=5432;dbname=$db;user=$user;password=$pass");
        return $dbh;
    } catch (Exception $e) {
        // Si echec de la connexion à la BDD
        echo "Unable to connect: " . $e->getMessage() . "<p>";
    }
}// connexion à la base de données examen_epreuve_e4

function list_postes() {
    $connexion = connexion();
    echo '<label>Selectionner un Poste : <select name ="poste_name">';
    $sql = $connexion->query("Select nomposte FROM statistiques.poste");
    while ($row = $sql->fetch()) {
        echo '<option value ="' . $row['nomposte'] . '">' . $row['nomposte'] . '</option>';
    }
    echo '</select></label>';
    
    $sql->closeCursor();
    $connexion = null;
} // Liste des postes de la table poste

function list_choix_jour() {
    echo '<label> Choisir le format : ';
    echo '<select name="mois_jour">';
    echo '<option value="mois_jour">Mois/jour</option>';
    echo '<option value="mois_jour_heure">Mois/jour/heure</option>';
    echo '<option value="jour_heure">Jour/heure</option>';
    echo '</select></label>';
} // Liste des differentes choix pour le graph

function historisation_poste($nomPoste){
    $connexion=connexion();
    $req = $connexion->query("SELECT * FROM statistiques.historisation WHERE nomposte ='".$nomPoste."'");
    $req->execute();
    $sql = $req->fetchAll();    
    foreach ($sql as $key){
        print($key['nomposte']);
        print($key['validite']);
        print($key['datesession']);
    }
    $connexion = null;
} // renvoie les valeurs du poste 

function generer_graph()
{
    $connexion = connexion();
    if ($_POST['mois_jour']){
        $typeHeure = 'second'; // récupếration Date/HH:MM:SS
    } else if ($_POST['mois_jour_heure'])  {
        $typeHeure = 'second'; // récupếration Date/HH:MM:SS
    } else if ($_POST['jour_heure']) {
        $typeHeure = 'jour_heure';
    } else {
        $typeHeure = 'second'; // récupếration Date/HH:MM:SS // par defaut
    }
    $req = "SELECT date_trunc('second',datesession),validite FROM statistiques.historisation WHERE nomposte ='".$_POST['poste_name']."'";
    
    $sql = $connexion->query($req);
    $sql->execute();
    $resultat = $sql->fetchAll(); 
    session_start();
    //print_r($resultat);
    if (isset($_SESSION['data']))
    {
        $_SESSION['data'] = array();        
    } 
    
        foreach ($resultat as $key){
             $_SESSION['data'][$key['date_trunc']] = $key['validite'];                    
    };
    //print_r($_SESSION['data']);
}




?>
